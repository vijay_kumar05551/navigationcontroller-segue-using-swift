//
//  ViewController.swift
//  NavigationController_Segue using swift
//
//  Created by OSX on 17/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var recordsTableView: UITableView!
    var showRecords:NSArray = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        showRecords = ["One", "Two", "Three", "Four", "Five"]
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return showRecords.count
    }

    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cellIdentifier:NSString = "recordsCell"
        let cell = tableView.dequeueReusableCellWithIdentifier(cellIdentifier as String) ?? UITableViewCell(style: .Default, reuseIdentifier: cellIdentifier as String)
        
        cell.accessoryType = UITableViewCellAccessoryType.DisclosureIndicator
        cell.textLabel?.text = showRecords.objectAtIndex(indexPath.row) as? String
        
        return cell;
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.performSegueWithIdentifier("moveToNext", sender: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "moveToNext" {
            let indexPath = recordsTableView.indexPathForSelectedRow
            let controller = segue.destinationViewController as! DetailViewController
            controller.showDetailStr = showRecords.objectAtIndex(indexPath!.row) as! String
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

