//
//  DetailViewController.swift
//  NavigationController_Segue using swift
//
//  Created by OSX on 17/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController {

    var showDetailStr:NSString = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        let showDetailLbl = UILabel()
        showDetailLbl.frame = CGRectMake(0, 0, self.view.frame.width, self.view.frame.height)
        showDetailLbl.font = UIFont.boldSystemFontOfSize(18)
        self.view.center = showDetailLbl.center
        self.view.addSubview(showDetailLbl)
        showDetailLbl.text = showDetailStr as String
        showDetailLbl.textAlignment = NSTextAlignment.Center
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
